const gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    clean = require('gulp-clean'),
    imagemin = require('gulp-imagemin'),
    browserSync = require('browser-sync').create();


const paths = {
    dev: {
        html: 'src/index.html',
        styles: 'src/scss/**/*.scss',
        script: 'src/script/**/*.js',
        image: 'src/image/**/*.jpg'
    },
    build: {
        root: 'build',
        styles: 'build/css',
        script: 'build/js',
        image: 'build/image/'
    }
};
const buildImages = () => (
    gulp.src(paths.dev.image)
        .pipe(imagemin())
        .pipe(gulp.dest(paths.build.image))
);

const buildHtml = () => (
    gulp.src(paths.dev.html)
        .pipe(gulp.dest(paths.build.root))
);

const buildStyles = () => (
    gulp.src(paths.dev.styles)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(paths.build.styles))
);

const buildJS = () => (
    gulp.src(paths.dev.script)
        .pipe(concat('main.js'))
        .pipe(gulp.dest(paths.build.script))
);

const serve = () => {
    gulp.watch(paths.dev.html, buildHtml);
    gulp.watch(paths.dev.styles, buildStyles);
    gulp.watch(paths.dev.script, buildJS);


    return browserSync.init({
        server: {
            baseDir: paths.build.root
        },
        files: [
            {match: paths.build.root, fn: this.reload}
        ]
    })
};

/***************** TASKS *****************/

gulp.task('clean', () => (
    gulp.src(paths.build.root, {allowEmpty: true})
        .pipe(clean())
));
gulp.task('build', gulp.series(
    buildHtml,
    buildStyles,
    buildJS,
    buildImages
));

gulp.task('serve', gulp.series(
    'clean',
    'build',
    serve
));
